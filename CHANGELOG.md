# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2025-02-17

### Changed

- bumped [axum](https://crates.io/crates/axum) from 0.3.x to 0.8.x
- bumped [http](https://crates.io/crates/http) from 0.2.x to 1.x.y
- add [http-body-util](https://crates.io/crates/http-body-util)
- bumped [hyper](https://crates.io/crates/hyper) from 0.14.x to 1.x.y
- bumped [tower](https://crates.io/crates/tower) from 0.4.x to 0.5.x

## [0.1.1] - 2021-11-25

### Fixed

- crates.io requires a maximum of 5 keywords for crate
