# tower-default-headers-rs [![Status](https://img.shields.io/badge/status-actively--developed-brightgreen)](https://gitlab.com/jokeyrhyme/tower-default-headers-rs) [![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/jokeyrhyme/tower-default-headers-rs?branch=main)](https://gitlab.com/jokeyrhyme/tower-default-headers-rs/-/pipelines?ref=main) [![Crates.io](https://img.shields.io/crates/v/tower-default-headers)](https://crates.io/crates/tower-default-headers) [![docs.rs](https://img.shields.io/docsrs/tower-default-headers)](https://docs.rs/tower-default-headers)

tower-compatible middleware to set default HTTP response headers

## see also

- tower: [source code](https://github.com/tower-rs/tower) [crate](https://crates.io/crates/tower)

- tokio blog post: [Inventing the `Service` trait](https://tokio.rs/blog/2021-05-14-inventing-the-service-trait)

- owasp-headers-rs: [source code](https://gitlab.com/jokeyrhyme/owasp-headers-rs) [crate](https://crates.io/crates/owasp-headers)
